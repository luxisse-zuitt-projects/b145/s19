let students = [];
let sectionedStudents = [];

function addStudent(name) {
	// call out the array and use the push() to add a new element inside the container.
	students.push(name);
	// display a return in the console
	console.log(name + ' has been added to the list of students');
}

addStudent('Zyrus');
addStudent('Marie');
addStudent('John');
console.log(students);

function countStudents() {
	// display the number of elements inside the array with the prescribed message.
	console.log('This Class has a total of ' + students.length + ' enrolled students');
}

// invocation
countStudents();


function printStudents() {
	// sort the elements inside the array in alphanumeric order.
	students = students.sort();
	// console.log(students);
	// we want to display each element in the console individually.
	students.forEach(function(student) {
		console.log(student);
	})
}

printStudents();

// anonymous function; only used once - function()
// each element - use singular form or array list

function findStudent(keyword) {
	// we have to describe each element inside the array individually.
	let matches = students.filter(function (student) {
		// we are assessing/evaluating if each element includes the keyword
		return student.toLowerCase().includes(keyword.toLowerCase());
	});
	console.log(typeof matches); //this is a checker
	// create a control structure that will give the proper response according to the result of the filter.

	if (matches.length === 1) {
		// if there are matches found for the keyword
		console.log(matches[0] + ' is enrolled.');
	} else if (matches.length > 1) {
		console.log('Multiple students match this keyword.')
	} else {
		// no results was found.
		console.log('No matches found for this keyword.');
	}
}

// Z === z
findStudent('r');
	
	// includes() method determines whether an array includes a certain value among its entries, returning TRUE or FALSE; needs the absolute value to equal TRUE

	// filter() method creates a new array with all elements that pass the test implemented by the provided function.

let fruits = ["banana", "grape", "orange", "strawberry"];
let match = fruits.filter(function(fruit) { return true; });
console.log(match.length);

function addSection(section) {
	// we will place each student inside the students array inside a new array called sectionStudents and we will add a section for each element inside the array.
	sectionStudents = students.map(function(student) {
		return student + ' is part of section: ' + section;
	})

	console.log(sectionStudents);
}

addSection('5');

// map() -> will create a new array populated with the elements that passes a condition on a given function/argument.

let fruitsList = ["banana", "orange", "apple", "grape"];
let mappedFruits = fruitsList.map(function(fruit) { return fruit + "a"; });
console.log(mappedFruits[0]);


function removeStudent(name) {
	// search for the index of John in the array
	let result = students.indexOf(name);
	console.log(result);
	
	// create a control structure if a result was found
	if (result >= 0) {
		// we will remove the element using the splice method.
		// students.splice(start, #OfElements/IndexesToRemove)
		students.splice(result, 1);
		console.log(students);
		console.log(name + ' was removed.');
	} else {
		console.log('No student was removed.');
	}
}

removeStudent('Zyrus');


console.log(students.indexOf('Zyrus'));
// indexOf will return the index number of a certain element.